
package it.ddway.tesoenti.business.service;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestServiceImplTest {

    @Test
    public void testSayHi() {
        TestServiceImpl testServiceImpl = new TestServiceImpl();
        String response = testServiceImpl.isUp("Sam");
        assertEquals("HelloWorldImpl not properly saying hi", "Hello Sam", response);
    }
}
