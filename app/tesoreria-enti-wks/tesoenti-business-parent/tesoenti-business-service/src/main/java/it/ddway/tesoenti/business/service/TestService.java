package it.ddway.tesoenti.business.service;

import javax.jws.WebService;

@WebService
public interface TestService {
    String isUp(String text);
}

