
package it.ddway.tesoenti.business.service;

import javax.jws.WebService;

@WebService(endpointInterface = "it.ddway.tesoenti.business.service.HelloWorld")
public class TestServiceImpl implements TestService {

    public String isUp(String text) {
        return "Hello " + text;
    }
}

