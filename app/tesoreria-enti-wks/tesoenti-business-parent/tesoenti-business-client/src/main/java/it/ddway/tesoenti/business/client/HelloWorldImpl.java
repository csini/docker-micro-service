
package it.ddway.tesoenti.business.client;

import javax.jws.WebService;

@WebService(endpointInterface = "it.ddway.tesoenti.business.client.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

