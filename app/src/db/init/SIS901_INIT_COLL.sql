PGDMP     *    7            
    t         	   tribudeda    9.1.1    9.3.1 F    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    166341 	   tribudeda    DATABASE     �   CREATE DATABASE tribudeda WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Italian, Italy' LC_CTYPE = 'Italian, Italy';
    DROP DATABASE tribudeda;
          	   tribudeda    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �            3079    11638    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    176            �            1259    166508    attore    TABLE     d   CREATE TABLE attore (
    id integer NOT NULL,
    denominazione character varying(255) NOT NULL
);
    DROP TABLE public.attore;
       public      	   tribudeda    false    5            �            1259    166506    attore_id_seq    SEQUENCE     o   CREATE SEQUENCE attore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.attore_id_seq;
       public    	   tribudeda    false    5    170            �           0    0    attore_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE attore_id_seq OWNED BY attore.id;
            public    	   tribudeda    false    169            �            1259    166532    attore_ruolo    TABLE     ]   CREATE TABLE attore_ruolo (
    id_ruolo integer NOT NULL,
    id_attore integer NOT NULL
);
     DROP TABLE public.attore_ruolo;
       public      	   tribudeda    false    5            �            1259    166466    ente    TABLE     F  CREATE TABLE ente (
    codice character varying(16) NOT NULL,
    nome character varying(150) NOT NULL,
    codice_istat character varying(50) NOT NULL,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone,
    CONSTRAINT ente_check CHECK ((NOT NULL::boolean))
);
    DROP TABLE public.ente;
       public      	   tribudeda    false    5            �            1259    166472    ente_gestore    TABLE     H  CREATE TABLE ente_gestore (
    codice character varying(16) NOT NULL,
    codice_ente character varying(16) NOT NULL,
    codice_iistat character varying(50),
    nome character varying(255) NOT NULL,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone NOT NULL
);
     DROP TABLE public.ente_gestore;
       public      	   tribudeda    false    5            �            1259    166500    ruolo    TABLE     Z   CREATE TABLE ruolo (
    id integer NOT NULL,
    denominazione character varying(255)
);
    DROP TABLE public.ruolo;
       public      	   tribudeda    false    5            �            1259    166498    ruolo_id_seq    SEQUENCE     n   CREATE SEQUENCE ruolo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.ruolo_id_seq;
       public    	   tribudeda    false    168    5            �           0    0    ruolo_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE ruolo_id_seq OWNED BY ruolo.id;
            public    	   tribudeda    false    167            �            1259    166522    ruolo_utente    TABLE     ]   CREATE TABLE ruolo_utente (
    id_utente integer NOT NULL,
    id_ruolo integer NOT NULL
);
     DROP TABLE public.ruolo_utente;
       public      	   tribudeda    false    5            �            1259    166477 	   struttura    TABLE       CREATE TABLE struttura (
    codice character varying(16) NOT NULL,
    livello smallint NOT NULL,
    denominazione character varying(255),
    id integer,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone,
    codice_ente character varying(16) NOT NULL,
    codice_struttura_padre character varying(16) NOT NULL,
    livello_struttura_padre smallint NOT NULL,
    ente_struttura_padre character varying(16) NOT NULL,
    codice_tipo_struttura character varying(16) NOT NULL
);
    DROP TABLE public.struttura;
       public      	   tribudeda    false    5            �            1259    166482    tipo_struttura    TABLE     �   CREATE TABLE tipo_struttura (
    codice character varying(16) NOT NULL,
    denominazione character varying(255),
    data_inizio_validita integer,
    data_fine_validita character varying(255)
);
 "   DROP TABLE public.tipo_struttura;
       public      	   tribudeda    false    5            �            1259    166516    usecase    TABLE     \   CREATE TABLE usecase (
    id integer NOT NULL,
    denominazione character varying(255)
);
    DROP TABLE public.usecase;
       public      	   tribudeda    false    5            �            1259    166527    usecase_attore    TABLE     a   CREATE TABLE usecase_attore (
    id_usecase integer NOT NULL,
    id_attore integer NOT NULL
);
 "   DROP TABLE public.usecase_attore;
       public      	   tribudeda    false    5            �            1259    166514    usecase_id_seq    SEQUENCE     p   CREATE SEQUENCE usecase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.usecase_id_seq;
       public    	   tribudeda    false    172    5            �           0    0    usecase_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE usecase_id_seq OWNED BY usecase.id;
            public    	   tribudeda    false    171            �            1259    166492    utente    TABLE     A  CREATE TABLE utente (
    id integer NOT NULL,
    codice_fiscale character varying(20) NOT NULL,
    matricola character varying(50),
    cognome character varying(100),
    nome character varying(100),
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone
);
    DROP TABLE public.utente;
       public      	   tribudeda    false    5            �           0    0    COLUMN utente.cognome    COMMENT     /   COMMENT ON COLUMN utente.cognome IS 'cognome';
            public    	   tribudeda    false    166            �            1259    166490    utente_id_seq    SEQUENCE     o   CREATE SEQUENCE utente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.utente_id_seq;
       public    	   tribudeda    false    5    166            �           0    0    utente_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE utente_id_seq OWNED BY utente.id;
            public    	   tribudeda    false    165                       2604    166511    id    DEFAULT     X   ALTER TABLE ONLY attore ALTER COLUMN id SET DEFAULT nextval('attore_id_seq'::regclass);
 8   ALTER TABLE public.attore ALTER COLUMN id DROP DEFAULT;
       public    	   tribudeda    false    169    170    170                       2604    166503    id    DEFAULT     V   ALTER TABLE ONLY ruolo ALTER COLUMN id SET DEFAULT nextval('ruolo_id_seq'::regclass);
 7   ALTER TABLE public.ruolo ALTER COLUMN id DROP DEFAULT;
       public    	   tribudeda    false    167    168    168                       2604    166519    id    DEFAULT     Z   ALTER TABLE ONLY usecase ALTER COLUMN id SET DEFAULT nextval('usecase_id_seq'::regclass);
 9   ALTER TABLE public.usecase ALTER COLUMN id DROP DEFAULT;
       public    	   tribudeda    false    172    171    172                       2604    166495    id    DEFAULT     X   ALTER TABLE ONLY utente ALTER COLUMN id SET DEFAULT nextval('utente_id_seq'::regclass);
 8   ALTER TABLE public.utente ALTER COLUMN id DROP DEFAULT;
       public    	   tribudeda    false    165    166    166            �          0    166508    attore 
   TABLE DATA               ,   COPY attore (id, denominazione) FROM stdin;
    public    	   tribudeda    false    170   (M       �           0    0    attore_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('attore_id_seq', 1, false);
            public    	   tribudeda    false    169            �          0    166532    attore_ruolo 
   TABLE DATA               4   COPY attore_ruolo (id_ruolo, id_attore) FROM stdin;
    public    	   tribudeda    false    175   EM       �          0    166466    ente 
   TABLE DATA               ]   COPY ente (codice, nome, codice_istat, data_inizio_validita, data_fine_validita) FROM stdin;
    public    	   tribudeda    false    161   bM       �          0    166472    ente_gestore 
   TABLE DATA               s   COPY ente_gestore (codice, codice_ente, codice_iistat, nome, data_inizio_validita, data_fine_validita) FROM stdin;
    public    	   tribudeda    false    162   M       �          0    166500    ruolo 
   TABLE DATA               +   COPY ruolo (id, denominazione) FROM stdin;
    public    	   tribudeda    false    168   �M       �           0    0    ruolo_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('ruolo_id_seq', 1, false);
            public    	   tribudeda    false    167            �          0    166522    ruolo_utente 
   TABLE DATA               4   COPY ruolo_utente (id_utente, id_ruolo) FROM stdin;
    public    	   tribudeda    false    173   �M       �          0    166477 	   struttura 
   TABLE DATA               �   COPY struttura (codice, livello, denominazione, id, data_inizio_validita, data_fine_validita, codice_ente, codice_struttura_padre, livello_struttura_padre, ente_struttura_padre, codice_tipo_struttura) FROM stdin;
    public    	   tribudeda    false    163   �M       �          0    166482    tipo_struttura 
   TABLE DATA               b   COPY tipo_struttura (codice, denominazione, data_inizio_validita, data_fine_validita) FROM stdin;
    public    	   tribudeda    false    164   �M       �          0    166516    usecase 
   TABLE DATA               -   COPY usecase (id, denominazione) FROM stdin;
    public    	   tribudeda    false    172   N       �          0    166527    usecase_attore 
   TABLE DATA               8   COPY usecase_attore (id_usecase, id_attore) FROM stdin;
    public    	   tribudeda    false    174   -N       �           0    0    usecase_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('usecase_id_seq', 1, false);
            public    	   tribudeda    false    171            �          0    166492    utente 
   TABLE DATA               q   COPY utente (id, codice_fiscale, matricola, cognome, nome, data_inizio_validita, data_fine_validita) FROM stdin;
    public    	   tribudeda    false    166   JN       �           0    0    utente_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('utente_id_seq', 1, false);
            public    	   tribudeda    false    165                       2606    166513    attore_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY attore
    ADD CONSTRAINT attore_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.attore DROP CONSTRAINT attore_pkey;
       public      	   tribudeda    false    170    170                       2606    166536    attore_ruolo_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT attore_ruolo_pkey PRIMARY KEY (id_ruolo, id_attore);
 H   ALTER TABLE ONLY public.attore_ruolo DROP CONSTRAINT attore_ruolo_pkey;
       public      	   tribudeda    false    175    175    175            
           2606    166476    ente_gestore_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY ente_gestore
    ADD CONSTRAINT ente_gestore_pkey PRIMARY KEY (codice);
 H   ALTER TABLE ONLY public.ente_gestore DROP CONSTRAINT ente_gestore_pkey;
       public      	   tribudeda    false    162    162                       2606    166471 	   ente_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY ente
    ADD CONSTRAINT ente_pkey PRIMARY KEY (codice);
 8   ALTER TABLE ONLY public.ente DROP CONSTRAINT ente_pkey;
       public      	   tribudeda    false    161    161                       2606    166505 
   ruolo_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY ruolo
    ADD CONSTRAINT ruolo_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.ruolo DROP CONSTRAINT ruolo_pkey;
       public      	   tribudeda    false    168    168                       2606    166526    ruolo_utente_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT ruolo_utente_pkey PRIMARY KEY (id_utente, id_ruolo);
 H   ALTER TABLE ONLY public.ruolo_utente DROP CONSTRAINT ruolo_utente_pkey;
       public      	   tribudeda    false    173    173    173                       2606    166481    struttura_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY struttura
    ADD CONSTRAINT struttura_pkey PRIMARY KEY (codice, livello, codice_ente);
 B   ALTER TABLE ONLY public.struttura DROP CONSTRAINT struttura_pkey;
       public      	   tribudeda    false    163    163    163    163                       2606    166489    tipo_struttura_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY tipo_struttura
    ADD CONSTRAINT tipo_struttura_pkey PRIMARY KEY (codice);
 L   ALTER TABLE ONLY public.tipo_struttura DROP CONSTRAINT tipo_struttura_pkey;
       public      	   tribudeda    false    164    164                       2606    166531    usecase_attore_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT usecase_attore_pkey PRIMARY KEY (id_usecase, id_attore);
 L   ALTER TABLE ONLY public.usecase_attore DROP CONSTRAINT usecase_attore_pkey;
       public      	   tribudeda    false    174    174    174                       2606    166521    usecase_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY usecase
    ADD CONSTRAINT usecase_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usecase DROP CONSTRAINT usecase_pkey;
       public      	   tribudeda    false    172    172                       2606    166497    utente_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY utente
    ADD CONSTRAINT utente_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.utente DROP CONSTRAINT utente_pkey;
       public      	   tribudeda    false    166    166                       1259    166538    ruolo_id    INDEX     1   CREATE INDEX ruolo_id ON ruolo USING btree (id);
    DROP INDEX public.ruolo_id;
       public      	   tribudeda    false    168                       1259    166539 
   usecase_id    INDEX     5   CREATE INDEX usecase_id ON usecase USING btree (id);
    DROP INDEX public.usecase_id;
       public      	   tribudeda    false    172                       1259    166537 	   utente_id    INDEX     3   CREATE INDEX utente_id ON utente USING btree (id);
    DROP INDEX public.utente_id;
       public      	   tribudeda    false    166            #           2606    166555    Raggruppamento Struttura    FK CONSTRAINT     �   ALTER TABLE ONLY struttura
    ADD CONSTRAINT "Raggruppamento Struttura" FOREIGN KEY (codice_tipo_struttura) REFERENCES tipo_struttura(codice);
 N   ALTER TABLE ONLY public.struttura DROP CONSTRAINT "Raggruppamento Struttura";
       public    	   tribudeda    false    1806    163    164            )           2606    166585    fkattore_ruo449017    FK CONSTRAINT     q   ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT fkattore_ruo449017 FOREIGN KEY (id_ruolo) REFERENCES ruolo(id);
 I   ALTER TABLE ONLY public.attore_ruolo DROP CONSTRAINT fkattore_ruo449017;
       public    	   tribudeda    false    175    168    1812            (           2606    166580    fkattore_ruo514021    FK CONSTRAINT     s   ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT fkattore_ruo514021 FOREIGN KEY (id_attore) REFERENCES attore(id);
 I   ALTER TABLE ONLY public.attore_ruolo DROP CONSTRAINT fkattore_ruo514021;
       public    	   tribudeda    false    1814    175    170                        2606    166540    fkente_gesto15537    FK CONSTRAINT     v   ALTER TABLE ONLY ente_gestore
    ADD CONSTRAINT fkente_gesto15537 FOREIGN KEY (codice_ente) REFERENCES ente(codice);
 H   ALTER TABLE ONLY public.ente_gestore DROP CONSTRAINT fkente_gesto15537;
       public    	   tribudeda    false    161    162    1800            $           2606    166560    fkruolo_uten125567    FK CONSTRAINT     q   ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT fkruolo_uten125567 FOREIGN KEY (id_ruolo) REFERENCES ruolo(id);
 I   ALTER TABLE ONLY public.ruolo_utente DROP CONSTRAINT fkruolo_uten125567;
       public    	   tribudeda    false    1812    168    173            %           2606    166565    fkruolo_uten890871    FK CONSTRAINT     s   ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT fkruolo_uten890871 FOREIGN KEY (id_utente) REFERENCES utente(id);
 I   ALTER TABLE ONLY public.ruolo_utente DROP CONSTRAINT fkruolo_uten890871;
       public    	   tribudeda    false    166    1809    173            '           2606    166575    fkusecase_at767590    FK CONSTRAINT     u   ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT fkusecase_at767590 FOREIGN KEY (id_attore) REFERENCES attore(id);
 K   ALTER TABLE ONLY public.usecase_attore DROP CONSTRAINT fkusecase_at767590;
       public    	   tribudeda    false    174    170    1814            &           2606    166570    fkusecase_at891564    FK CONSTRAINT     w   ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT fkusecase_at891564 FOREIGN KEY (id_usecase) REFERENCES usecase(id);
 K   ALTER TABLE ONLY public.usecase_attore DROP CONSTRAINT fkusecase_at891564;
       public    	   tribudeda    false    174    172    1817            "           2606    166550    parent    FK CONSTRAINT     �   ALTER TABLE ONLY struttura
    ADD CONSTRAINT parent FOREIGN KEY (codice_struttura_padre, livello_struttura_padre, ente_struttura_padre) REFERENCES struttura(codice, livello, codice_ente);
 :   ALTER TABLE ONLY public.struttura DROP CONSTRAINT parent;
       public    	   tribudeda    false    163    1804    163    163    163    163    163            !           2606    166545    struttura_ente    FK CONSTRAINT     p   ALTER TABLE ONLY struttura
    ADD CONSTRAINT struttura_ente FOREIGN KEY (codice_ente) REFERENCES ente(codice);
 B   ALTER TABLE ONLY public.struttura DROP CONSTRAINT struttura_ente;
       public    	   tribudeda    false    163    1800    161            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �     