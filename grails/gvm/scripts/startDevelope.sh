#!/bin/sh	

if [ "$#" -gt "0" ]
 docker run -i -t -p 8080:8080 --rm -v $HOME/workspace/apps/grails/$1:/app onesysadmin/grails
elif
 docker run -i -t -p 8080:8080 --rm -v $HOME/workspace/apps/grails:/app onesysadmin/grails
fi
