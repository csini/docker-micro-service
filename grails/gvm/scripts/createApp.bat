rem setlocal enabledelayedexpansion

set applicativo=%1
echo %applicativo%
@echo off

rem @echo off
rem scripts\setenv

if %applicativo%. EQU . (
   echo "passare nome applicativo"
  GOTO end
)


FOR /f "tokens=*" %%i IN ('docker-machine env default') DO %%i

docker run -i -t -p 80:80 --rm -v /apps/grails/it/cgarret/tut:/app onesysadmin/grails grails create-app %applicativo%


:end 

