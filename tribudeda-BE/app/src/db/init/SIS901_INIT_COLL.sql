--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.1
-- Dumped by pg_dump version 9.3.1
-- Started on 2016-11-30 15:06:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 176 (class 3079 OID 11638)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1942 (class 0 OID 0)
-- Dependencies: 176
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 166508)
-- Name: attore; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE attore (
    id integer NOT NULL,
    denominazione character varying(255) NOT NULL
);


ALTER TABLE public.attore OWNER TO tribudeda;

--
-- TOC entry 169 (class 1259 OID 166506)
-- Name: attore_id_seq; Type: SEQUENCE; Schema: public; Owner: tribudeda
--

CREATE SEQUENCE attore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attore_id_seq OWNER TO tribudeda;

--
-- TOC entry 1943 (class 0 OID 0)
-- Dependencies: 169
-- Name: attore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tribudeda
--

ALTER SEQUENCE attore_id_seq OWNED BY attore.id;


--
-- TOC entry 175 (class 1259 OID 166532)
-- Name: attore_ruolo; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE attore_ruolo (
    id_ruolo integer NOT NULL,
    id_attore integer NOT NULL
);


ALTER TABLE public.attore_ruolo OWNER TO tribudeda;

--
-- TOC entry 161 (class 1259 OID 166466)
-- Name: ente; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE ente (
    codice character varying(16) NOT NULL,
    nome character varying(150) NOT NULL,
    codice_istat character varying(50) NOT NULL,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone,
    CONSTRAINT ente_check CHECK ((NOT NULL::boolean))
);


ALTER TABLE public.ente OWNER TO tribudeda;

--
-- TOC entry 162 (class 1259 OID 166472)
-- Name: ente_gestore; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE ente_gestore (
    codice character varying(16) NOT NULL,
    codice_ente character varying(16) NOT NULL,
    codice_iistat character varying(50),
    nome character varying(255) NOT NULL,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone NOT NULL
);


ALTER TABLE public.ente_gestore OWNER TO tribudeda;

--
-- TOC entry 168 (class 1259 OID 166500)
-- Name: ruolo; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE ruolo (
    id integer NOT NULL,
    denominazione character varying(255)
);


ALTER TABLE public.ruolo OWNER TO tribudeda;

--
-- TOC entry 167 (class 1259 OID 166498)
-- Name: ruolo_id_seq; Type: SEQUENCE; Schema: public; Owner: tribudeda
--

CREATE SEQUENCE ruolo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ruolo_id_seq OWNER TO tribudeda;

--
-- TOC entry 1944 (class 0 OID 0)
-- Dependencies: 167
-- Name: ruolo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tribudeda
--

ALTER SEQUENCE ruolo_id_seq OWNED BY ruolo.id;


--
-- TOC entry 173 (class 1259 OID 166522)
-- Name: ruolo_utente; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE ruolo_utente (
    id_utente integer NOT NULL,
    id_ruolo integer NOT NULL
);


ALTER TABLE public.ruolo_utente OWNER TO tribudeda;

--
-- TOC entry 163 (class 1259 OID 166477)
-- Name: struttura; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE struttura (
    codice character varying(16) NOT NULL,
    livello smallint NOT NULL,
    denominazione character varying(255),
    id integer,
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone,
    codice_ente character varying(16) NOT NULL,
    codice_struttura_padre character varying(16) NOT NULL,
    livello_struttura_padre smallint NOT NULL,
    ente_struttura_padre character varying(16) NOT NULL,
    codice_tipo_struttura character varying(16) NOT NULL
);


ALTER TABLE public.struttura OWNER TO tribudeda;

--
-- TOC entry 164 (class 1259 OID 166482)
-- Name: tipo_struttura; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE tipo_struttura (
    codice character varying(16) NOT NULL,
    denominazione character varying(255),
    data_inizio_validita integer,
    data_fine_validita character varying(255)
);


ALTER TABLE public.tipo_struttura OWNER TO tribudeda;

--
-- TOC entry 172 (class 1259 OID 166516)
-- Name: usecase; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE usecase (
    id integer NOT NULL,
    denominazione character varying(255)
);


ALTER TABLE public.usecase OWNER TO tribudeda;

--
-- TOC entry 174 (class 1259 OID 166527)
-- Name: usecase_attore; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE usecase_attore (
    id_usecase integer NOT NULL,
    id_attore integer NOT NULL
);


ALTER TABLE public.usecase_attore OWNER TO tribudeda;

--
-- TOC entry 171 (class 1259 OID 166514)
-- Name: usecase_id_seq; Type: SEQUENCE; Schema: public; Owner: tribudeda
--

CREATE SEQUENCE usecase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usecase_id_seq OWNER TO tribudeda;

--
-- TOC entry 1945 (class 0 OID 0)
-- Dependencies: 171
-- Name: usecase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tribudeda
--

ALTER SEQUENCE usecase_id_seq OWNED BY usecase.id;


--
-- TOC entry 166 (class 1259 OID 166492)
-- Name: utente; Type: TABLE; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE TABLE utente (
    id integer NOT NULL,
    codice_fiscale character varying(20) NOT NULL,
    matricola character varying(50),
    cognome character varying(100),
    nome character varying(100),
    data_inizio_validita timestamp without time zone NOT NULL,
    data_fine_validita timestamp without time zone
);


ALTER TABLE public.utente OWNER TO tribudeda;

--
-- TOC entry 1946 (class 0 OID 0)
-- Dependencies: 166
-- Name: COLUMN utente.cognome; Type: COMMENT; Schema: public; Owner: tribudeda
--

COMMENT ON COLUMN utente.cognome IS 'cognome';


--
-- TOC entry 165 (class 1259 OID 166490)
-- Name: utente_id_seq; Type: SEQUENCE; Schema: public; Owner: tribudeda
--

CREATE SEQUENCE utente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utente_id_seq OWNER TO tribudeda;

--
-- TOC entry 1947 (class 0 OID 0)
-- Dependencies: 165
-- Name: utente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tribudeda
--

ALTER SEQUENCE utente_id_seq OWNED BY utente.id;


--
-- TOC entry 1797 (class 2604 OID 166511)
-- Name: id; Type: DEFAULT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY attore ALTER COLUMN id SET DEFAULT nextval('attore_id_seq'::regclass);


--
-- TOC entry 1796 (class 2604 OID 166503)
-- Name: id; Type: DEFAULT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY ruolo ALTER COLUMN id SET DEFAULT nextval('ruolo_id_seq'::regclass);


--
-- TOC entry 1798 (class 2604 OID 166519)
-- Name: id; Type: DEFAULT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY usecase ALTER COLUMN id SET DEFAULT nextval('usecase_id_seq'::regclass);


--
-- TOC entry 1795 (class 2604 OID 166495)
-- Name: id; Type: DEFAULT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY utente ALTER COLUMN id SET DEFAULT nextval('utente_id_seq'::regclass);


--
-- TOC entry 1814 (class 2606 OID 166513)
-- Name: attore_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY attore
    ADD CONSTRAINT attore_pkey PRIMARY KEY (id);


--
-- TOC entry 1823 (class 2606 OID 166536)
-- Name: attore_ruolo_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT attore_ruolo_pkey PRIMARY KEY (id_ruolo, id_attore);


--
-- TOC entry 1802 (class 2606 OID 166476)
-- Name: ente_gestore_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY ente_gestore
    ADD CONSTRAINT ente_gestore_pkey PRIMARY KEY (codice);


--
-- TOC entry 1800 (class 2606 OID 166471)
-- Name: ente_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY ente
    ADD CONSTRAINT ente_pkey PRIMARY KEY (codice);


--
-- TOC entry 1812 (class 2606 OID 166505)
-- Name: ruolo_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY ruolo
    ADD CONSTRAINT ruolo_pkey PRIMARY KEY (id);


--
-- TOC entry 1819 (class 2606 OID 166526)
-- Name: ruolo_utente_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT ruolo_utente_pkey PRIMARY KEY (id_utente, id_ruolo);


--
-- TOC entry 1804 (class 2606 OID 166481)
-- Name: struttura_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY struttura
    ADD CONSTRAINT struttura_pkey PRIMARY KEY (codice, livello, codice_ente);


--
-- TOC entry 1806 (class 2606 OID 166489)
-- Name: tipo_struttura_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY tipo_struttura
    ADD CONSTRAINT tipo_struttura_pkey PRIMARY KEY (codice);


--
-- TOC entry 1821 (class 2606 OID 166531)
-- Name: usecase_attore_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT usecase_attore_pkey PRIMARY KEY (id_usecase, id_attore);


--
-- TOC entry 1817 (class 2606 OID 166521)
-- Name: usecase_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY usecase
    ADD CONSTRAINT usecase_pkey PRIMARY KEY (id);


--
-- TOC entry 1809 (class 2606 OID 166497)
-- Name: utente_pkey; Type: CONSTRAINT; Schema: public; Owner: tribudeda; Tablespace: 
--

ALTER TABLE ONLY utente
    ADD CONSTRAINT utente_pkey PRIMARY KEY (id);


--
-- TOC entry 1810 (class 1259 OID 166538)
-- Name: ruolo_id; Type: INDEX; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE INDEX ruolo_id ON ruolo USING btree (id);


--
-- TOC entry 1815 (class 1259 OID 166539)
-- Name: usecase_id; Type: INDEX; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE INDEX usecase_id ON usecase USING btree (id);


--
-- TOC entry 1807 (class 1259 OID 166537)
-- Name: utente_id; Type: INDEX; Schema: public; Owner: tribudeda; Tablespace: 
--

CREATE INDEX utente_id ON utente USING btree (id);


--
-- TOC entry 1827 (class 2606 OID 166555)
-- Name: Raggruppamento Struttura; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY struttura
    ADD CONSTRAINT "Raggruppamento Struttura" FOREIGN KEY (codice_tipo_struttura) REFERENCES tipo_struttura(codice);


--
-- TOC entry 1833 (class 2606 OID 166585)
-- Name: fkattore_ruo449017; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT fkattore_ruo449017 FOREIGN KEY (id_ruolo) REFERENCES ruolo(id);


--
-- TOC entry 1832 (class 2606 OID 166580)
-- Name: fkattore_ruo514021; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY attore_ruolo
    ADD CONSTRAINT fkattore_ruo514021 FOREIGN KEY (id_attore) REFERENCES attore(id);


--
-- TOC entry 1824 (class 2606 OID 166540)
-- Name: fkente_gesto15537; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY ente_gestore
    ADD CONSTRAINT fkente_gesto15537 FOREIGN KEY (codice_ente) REFERENCES ente(codice);


--
-- TOC entry 1828 (class 2606 OID 166560)
-- Name: fkruolo_uten125567; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT fkruolo_uten125567 FOREIGN KEY (id_ruolo) REFERENCES ruolo(id);


--
-- TOC entry 1829 (class 2606 OID 166565)
-- Name: fkruolo_uten890871; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY ruolo_utente
    ADD CONSTRAINT fkruolo_uten890871 FOREIGN KEY (id_utente) REFERENCES utente(id);


--
-- TOC entry 1831 (class 2606 OID 166575)
-- Name: fkusecase_at767590; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT fkusecase_at767590 FOREIGN KEY (id_attore) REFERENCES attore(id);


--
-- TOC entry 1830 (class 2606 OID 166570)
-- Name: fkusecase_at891564; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY usecase_attore
    ADD CONSTRAINT fkusecase_at891564 FOREIGN KEY (id_usecase) REFERENCES usecase(id);


--
-- TOC entry 1826 (class 2606 OID 166550)
-- Name: parent; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY struttura
    ADD CONSTRAINT parent FOREIGN KEY (codice_struttura_padre, livello_struttura_padre, ente_struttura_padre) REFERENCES struttura(codice, livello, codice_ente);


--
-- TOC entry 1825 (class 2606 OID 166545)
-- Name: struttura_ente; Type: FK CONSTRAINT; Schema: public; Owner: tribudeda
--

ALTER TABLE ONLY struttura
    ADD CONSTRAINT struttura_ente FOREIGN KEY (codice_ente) REFERENCES ente(codice);


--
-- TOC entry 1941 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-11-30 15:06:09

--
-- PostgreSQL database dump complete
--

