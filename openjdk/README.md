Supported tags and respective Dockerfile links
6.0.47-jre7, 6.0-jre7, 6-jre7, 6.0.47, 6.0, 6 (6/jre7/Dockerfile)
6.0.47-jre8, 6.0-jre8, 6-jre8 (6/jre8/Dockerfile)
7.0.72-jre7, 7.0-jre7, 7-jre7, 7.0.72, 7.0, 7 (7/jre7/Dockerfile)
7.0.72-jre7-alpine, 7.0-jre7-alpine, 7-jre7-alpine, 7.0.72-alpine, 7.0-alpine, 7-alpine (7/jre7-alpine/Dockerfile)
7.0.72-jre8, 7.0-jre8, 7-jre8 (7/jre8/Dockerfile)
7.0.72-jre8-alpine, 7.0-jre8-alpine, 7-jre8-alpine (7/jre8-alpine/Dockerfile)
8.0.38-jre7, 8.0-jre7, 8-jre7, jre7, 8.0.38, 8.0, 8, latest (8.0/jre7/Dockerfile)
8.0.38-jre7-alpine, 8.0-jre7-alpine, 8-jre7-alpine, jre7-alpine, 8.0.38-alpine, 8.0-alpine, 8-alpine, alpine (8.0/jre7-alpine/Dockerfile)
8.0.38-jre8, 8.0-jre8, 8-jre8, jre8 (8.0/jre8/Dockerfile)
8.0.38-jre8-alpine, 8.0-jre8-alpine, 8-jre8-alpine, jre8-alpine (8.0/jre8-alpine/Dockerfile)
8.5.6-jre8, 8.5-jre8, 8.5.6, 8.5 (8.5/jre8/Dockerfile)
8.5.6-jre8-alpine, 8.5-jre8-alpine, 8.5.6-alpine, 8.5-alpine (8.5/jre8-alpine/Dockerfile)
9.0.0.M11-jre8, 9.0.0-jre8, 9.0-jre8, 9-jre8, 9.0.0.M11, 9.0.0, 9.0, 9 (9.0/jre8/Dockerfile)
9.0.0.M11-jre8-alpine, 9.0.0-jre8-alpine, 9.0-jre8-alpine, 9-jre8-alpine, 9.0.0.M11-alpine, 9.0.0-alpine, 9.0-alpine, 9-alpine (9.0/jre8-alpine/Dockerfile)
For more information about this image and its history, please see the relevant manifest file (library/tomcat). This image is updated via pull requests to the docker-library/official-images GitHub repo.

For detailed information about the virtual/transfer sizes and individual layers of each of the above supported tags, please see the repos/tomcat/tag-details.md file in the docker-library/repo-info GitHub repo.


CATALINA_BASE:   /usr/local/tomcat
CATALINA_HOME:   /usr/local/tomcat
CATALINA_TMPDIR: /usr/local/tomcat/temp
JRE_HOME:        /usr
CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar

The configuration files are available in /usr/local/tomcat/conf/. By default, no user is included in the "manager-gui" role required to operate the "/manager/html" web application. If you wish to use this app, you must define such a user in tomcat-users.xml.
