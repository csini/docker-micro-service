@ECHO OFF
REM The script sets environment variables helpful for PostgreSQL

@SET PATH="C:\workspace\apps\PostgreSQL\9.1\bin";%PATH%
@SET PGDATA=C:\workspace\apps\PostgreSQL\9.1\data
@SET PGDATABASE=postgres
@SET PGUSER=postgres
@SET PGPORT=5432
@SET PGLOCALEDIR=C:\workspace\apps\PostgreSQL\9.1\share\locale

                          